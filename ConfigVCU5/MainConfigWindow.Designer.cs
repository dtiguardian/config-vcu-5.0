﻿namespace ConfigVCU5
{
    partial class MainConfigWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.VCUSNLabel = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.connectF1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.openValveF2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeValveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToVCUToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zeroCountToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.systemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.firmwareUpdateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configBlutoothModuleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.configExternalModuleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.auxInputsCalibrationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetVCUToDefaultsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enableFeaturesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.proToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eliteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.saveVCUDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.templateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inputsOutputsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.auxiliary1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.auxiliary2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.auxiliary3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.auxiliary4ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.auxiliary5ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.auxiliary6ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.auxiliary7ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.auxiliary8ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.valveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pTOToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rPMToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.remoteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.keyfobToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.masterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eventLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.conditionCodesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tiltToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpTopicsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkForUpdatesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutDTIGuardianConfigApplicantToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ValveVCUInfoBox = new System.Windows.Forms.GroupBox();
            this.ValveSNValue = new System.Windows.Forms.Label();
            this.VCUSNValue = new System.Windows.Forms.Label();
            this.ValveSNLabel = new System.Windows.Forms.Label();
            this.ValveInteractionBox = new System.Windows.Forms.GroupBox();
            this.StatusBox = new System.Windows.Forms.GroupBox();
            this.ValveDataBox = new System.Windows.Forms.GroupBox();
            this.ConnectionBox = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.ConfigBox = new System.Windows.Forms.GroupBox();
            this.UserInfoBox = new System.Windows.Forms.GroupBox();
            this.UserTypeValue = new System.Windows.Forms.Label();
            this.UserValue = new System.Windows.Forms.Label();
            this.UserTypeLabel = new System.Windows.Forms.Label();
            this.UserLabel = new System.Windows.Forms.Label();
            this.FirmwareLabel = new System.Windows.Forms.Label();
            this.HardwareLabel = new System.Windows.Forms.Label();
            this.HardwareVersion = new System.Windows.Forms.Label();
            this.FirmwareVersion = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.menuStrip1.SuspendLayout();
            this.ValveVCUInfoBox.SuspendLayout();
            this.ValveInteractionBox.SuspendLayout();
            this.StatusBox.SuspendLayout();
            this.ValveDataBox.SuspendLayout();
            this.ConnectionBox.SuspendLayout();
            this.ConfigBox.SuspendLayout();
            this.UserInfoBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // VCUSNLabel
            // 
            this.VCUSNLabel.AutoSize = true;
            this.VCUSNLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VCUSNLabel.ForeColor = System.Drawing.SystemColors.InfoText;
            this.VCUSNLabel.Location = new System.Drawing.Point(13, 16);
            this.VCUSNLabel.Name = "VCUSNLabel";
            this.VCUSNLabel.Size = new System.Drawing.Size(119, 13);
            this.VCUSNLabel.TabIndex = 0;
            this.VCUSNLabel.Text = "VCU Serial Number:";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.templateToolStripMenuItem,
            this.inputsOutputsToolStripMenuItem,
            this.valveToolStripMenuItem,
            this.pTOToolStripMenuItem,
            this.rPMToolStripMenuItem,
            this.remoteToolStripMenuItem,
            this.eventLogToolStripMenuItem,
            this.conditionCodesToolStripMenuItem,
            this.tiltToolStripMenuItem,
            this.helpToolStripMenuItem,
            this.testToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1053, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.connectF1ToolStripMenuItem,
            this.toolStripSeparator3,
            this.openValveF2ToolStripMenuItem,
            this.closeValveToolStripMenuItem,
            this.saveToVCUToolStripMenuItem,
            this.zeroCountToolStripMenuItem,
            this.toolStripSeparator4,
            this.systemToolStripMenuItem,
            this.toolStripSeparator5,
            this.closeToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // connectF1ToolStripMenuItem
            // 
            this.connectF1ToolStripMenuItem.Name = "connectF1ToolStripMenuItem";
            this.connectF1ToolStripMenuItem.ShortcutKeyDisplayString = "F1";
            this.connectF1ToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.connectF1ToolStripMenuItem.Text = "Connect";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(154, 6);
            // 
            // openValveF2ToolStripMenuItem
            // 
            this.openValveF2ToolStripMenuItem.Name = "openValveF2ToolStripMenuItem";
            this.openValveF2ToolStripMenuItem.ShortcutKeyDisplayString = "F2";
            this.openValveF2ToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.openValveF2ToolStripMenuItem.Text = "Open Valve";
            // 
            // closeValveToolStripMenuItem
            // 
            this.closeValveToolStripMenuItem.Name = "closeValveToolStripMenuItem";
            this.closeValveToolStripMenuItem.ShortcutKeyDisplayString = "F3";
            this.closeValveToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.closeValveToolStripMenuItem.Text = "Close Valve";
            // 
            // saveToVCUToolStripMenuItem
            // 
            this.saveToVCUToolStripMenuItem.Name = "saveToVCUToolStripMenuItem";
            this.saveToVCUToolStripMenuItem.ShortcutKeyDisplayString = "F4";
            this.saveToVCUToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.saveToVCUToolStripMenuItem.Text = "Save to VCU";
            // 
            // zeroCountToolStripMenuItem
            // 
            this.zeroCountToolStripMenuItem.Name = "zeroCountToolStripMenuItem";
            this.zeroCountToolStripMenuItem.ShortcutKeyDisplayString = "F5";
            this.zeroCountToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.zeroCountToolStripMenuItem.Text = "Zero Count";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(154, 6);
            // 
            // systemToolStripMenuItem
            // 
            this.systemToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.firmwareUpdateToolStripMenuItem,
            this.configBlutoothModuleToolStripMenuItem,
            this.toolStripSeparator2,
            this.configExternalModuleToolStripMenuItem,
            this.auxInputsCalibrationToolStripMenuItem,
            this.resetVCUToDefaultsToolStripMenuItem,
            this.enableFeaturesToolStripMenuItem,
            this.toolStripSeparator1,
            this.saveVCUDataToolStripMenuItem});
            this.systemToolStripMenuItem.Name = "systemToolStripMenuItem";
            this.systemToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.systemToolStripMenuItem.Text = "System";
            // 
            // firmwareUpdateToolStripMenuItem
            // 
            this.firmwareUpdateToolStripMenuItem.Name = "firmwareUpdateToolStripMenuItem";
            this.firmwareUpdateToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.firmwareUpdateToolStripMenuItem.Text = "Firmware Update";
            // 
            // configBlutoothModuleToolStripMenuItem
            // 
            this.configBlutoothModuleToolStripMenuItem.Name = "configBlutoothModuleToolStripMenuItem";
            this.configBlutoothModuleToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.configBlutoothModuleToolStripMenuItem.Text = "Config Blutooth Module";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(200, 6);
            // 
            // configExternalModuleToolStripMenuItem
            // 
            this.configExternalModuleToolStripMenuItem.Name = "configExternalModuleToolStripMenuItem";
            this.configExternalModuleToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.configExternalModuleToolStripMenuItem.Text = "Config External Module";
            // 
            // auxInputsCalibrationToolStripMenuItem
            // 
            this.auxInputsCalibrationToolStripMenuItem.Name = "auxInputsCalibrationToolStripMenuItem";
            this.auxInputsCalibrationToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.auxInputsCalibrationToolStripMenuItem.Text = "Aux Inputs Calibration";
            // 
            // resetVCUToDefaultsToolStripMenuItem
            // 
            this.resetVCUToDefaultsToolStripMenuItem.Name = "resetVCUToDefaultsToolStripMenuItem";
            this.resetVCUToDefaultsToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.resetVCUToDefaultsToolStripMenuItem.Text = "ResetVCU to Defaults";
            // 
            // enableFeaturesToolStripMenuItem
            // 
            this.enableFeaturesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.proToolStripMenuItem,
            this.eliteToolStripMenuItem});
            this.enableFeaturesToolStripMenuItem.Name = "enableFeaturesToolStripMenuItem";
            this.enableFeaturesToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.enableFeaturesToolStripMenuItem.Text = "Enable Features";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(119, 22);
            this.toolStripMenuItem2.Text = "5.0 Basic";
            // 
            // proToolStripMenuItem
            // 
            this.proToolStripMenuItem.Name = "proToolStripMenuItem";
            this.proToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.proToolStripMenuItem.Text = "5.0 Pro";
            // 
            // eliteToolStripMenuItem
            // 
            this.eliteToolStripMenuItem.Name = "eliteToolStripMenuItem";
            this.eliteToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.eliteToolStripMenuItem.Text = "5.0 Elite";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(200, 6);
            // 
            // saveVCUDataToolStripMenuItem
            // 
            this.saveVCUDataToolStripMenuItem.Name = "saveVCUDataToolStripMenuItem";
            this.saveVCUDataToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.saveVCUDataToolStripMenuItem.Text = "Save VCU Data";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(154, 6);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.closeToolStripMenuItem.Text = "Close";
            // 
            // templateToolStripMenuItem
            // 
            this.templateToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadToolStripMenuItem,
            this.saveToolStripMenuItem});
            this.templateToolStripMenuItem.Name = "templateToolStripMenuItem";
            this.templateToolStripMenuItem.Size = new System.Drawing.Size(68, 20);
            this.templateToolStripMenuItem.Text = "Template";
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.loadToolStripMenuItem.Text = "Load";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.saveToolStripMenuItem.Text = "Save";
            // 
            // inputsOutputsToolStripMenuItem
            // 
            this.inputsOutputsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.auxiliary1ToolStripMenuItem,
            this.auxiliary2ToolStripMenuItem,
            this.auxiliary3ToolStripMenuItem,
            this.auxiliary4ToolStripMenuItem,
            this.auxiliary5ToolStripMenuItem,
            this.auxiliary6ToolStripMenuItem,
            this.auxiliary7ToolStripMenuItem,
            this.auxiliary8ToolStripMenuItem});
            this.inputsOutputsToolStripMenuItem.Name = "inputsOutputsToolStripMenuItem";
            this.inputsOutputsToolStripMenuItem.Size = new System.Drawing.Size(101, 20);
            this.inputsOutputsToolStripMenuItem.Text = "Inputs & Outputs";
            // 
            // auxiliary1ToolStripMenuItem
            // 
            this.auxiliary1ToolStripMenuItem.Name = "auxiliary1ToolStripMenuItem";
            this.auxiliary1ToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+1";
            this.auxiliary1ToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.auxiliary1ToolStripMenuItem.Text = "Auxiliary 1";
            // 
            // auxiliary2ToolStripMenuItem
            // 
            this.auxiliary2ToolStripMenuItem.Name = "auxiliary2ToolStripMenuItem";
            this.auxiliary2ToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+2";
            this.auxiliary2ToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.auxiliary2ToolStripMenuItem.Text = "Auxiliary 2";
            // 
            // auxiliary3ToolStripMenuItem
            // 
            this.auxiliary3ToolStripMenuItem.Name = "auxiliary3ToolStripMenuItem";
            this.auxiliary3ToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+3";
            this.auxiliary3ToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.auxiliary3ToolStripMenuItem.Text = "Auxiliary 3";
            // 
            // auxiliary4ToolStripMenuItem
            // 
            this.auxiliary4ToolStripMenuItem.Name = "auxiliary4ToolStripMenuItem";
            this.auxiliary4ToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+4";
            this.auxiliary4ToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.auxiliary4ToolStripMenuItem.Text = "Auxiliary 4";
            // 
            // auxiliary5ToolStripMenuItem
            // 
            this.auxiliary5ToolStripMenuItem.Name = "auxiliary5ToolStripMenuItem";
            this.auxiliary5ToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+5";
            this.auxiliary5ToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.auxiliary5ToolStripMenuItem.Text = "Auxiliary 5";
            // 
            // auxiliary6ToolStripMenuItem
            // 
            this.auxiliary6ToolStripMenuItem.Name = "auxiliary6ToolStripMenuItem";
            this.auxiliary6ToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+6";
            this.auxiliary6ToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.auxiliary6ToolStripMenuItem.Text = "Auxiliary 6";
            // 
            // auxiliary7ToolStripMenuItem
            // 
            this.auxiliary7ToolStripMenuItem.Name = "auxiliary7ToolStripMenuItem";
            this.auxiliary7ToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+7";
            this.auxiliary7ToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.auxiliary7ToolStripMenuItem.Text = "Auxiliary 7";
            // 
            // auxiliary8ToolStripMenuItem
            // 
            this.auxiliary8ToolStripMenuItem.Name = "auxiliary8ToolStripMenuItem";
            this.auxiliary8ToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+8";
            this.auxiliary8ToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.auxiliary8ToolStripMenuItem.Text = "Auxiliary 8";
            // 
            // valveToolStripMenuItem
            // 
            this.valveToolStripMenuItem.Name = "valveToolStripMenuItem";
            this.valveToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.valveToolStripMenuItem.Text = "Valve";
            // 
            // pTOToolStripMenuItem
            // 
            this.pTOToolStripMenuItem.Name = "pTOToolStripMenuItem";
            this.pTOToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
            this.pTOToolStripMenuItem.Text = "PTO";
            // 
            // rPMToolStripMenuItem
            // 
            this.rPMToolStripMenuItem.Name = "rPMToolStripMenuItem";
            this.rPMToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.rPMToolStripMenuItem.Text = "RPM";
            // 
            // remoteToolStripMenuItem
            // 
            this.remoteToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.keyfobToolStripMenuItem,
            this.masterToolStripMenuItem});
            this.remoteToolStripMenuItem.Name = "remoteToolStripMenuItem";
            this.remoteToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.remoteToolStripMenuItem.Text = "Remote";
            // 
            // keyfobToolStripMenuItem
            // 
            this.keyfobToolStripMenuItem.Name = "keyfobToolStripMenuItem";
            this.keyfobToolStripMenuItem.Size = new System.Drawing.Size(111, 22);
            this.keyfobToolStripMenuItem.Text = "Keyfob";
            // 
            // masterToolStripMenuItem
            // 
            this.masterToolStripMenuItem.Name = "masterToolStripMenuItem";
            this.masterToolStripMenuItem.Size = new System.Drawing.Size(111, 22);
            this.masterToolStripMenuItem.Text = "Master";
            // 
            // eventLogToolStripMenuItem
            // 
            this.eventLogToolStripMenuItem.Name = "eventLogToolStripMenuItem";
            this.eventLogToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.eventLogToolStripMenuItem.Text = "Event Log";
            // 
            // conditionCodesToolStripMenuItem
            // 
            this.conditionCodesToolStripMenuItem.Name = "conditionCodesToolStripMenuItem";
            this.conditionCodesToolStripMenuItem.Size = new System.Drawing.Size(108, 20);
            this.conditionCodesToolStripMenuItem.Text = "Condition Codes";
            // 
            // tiltToolStripMenuItem
            // 
            this.tiltToolStripMenuItem.Name = "tiltToolStripMenuItem";
            this.tiltToolStripMenuItem.Size = new System.Drawing.Size(36, 20);
            this.tiltToolStripMenuItem.Text = "Tilt";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpTopicsToolStripMenuItem,
            this.checkForUpdatesToolStripMenuItem,
            this.aboutDTIGuardianConfigApplicantToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // helpTopicsToolStripMenuItem
            // 
            this.helpTopicsToolStripMenuItem.Name = "helpTopicsToolStripMenuItem";
            this.helpTopicsToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.helpTopicsToolStripMenuItem.Text = "Help Topics";
            // 
            // checkForUpdatesToolStripMenuItem
            // 
            this.checkForUpdatesToolStripMenuItem.Name = "checkForUpdatesToolStripMenuItem";
            this.checkForUpdatesToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.checkForUpdatesToolStripMenuItem.Text = "Check for Updates";
            // 
            // aboutDTIGuardianConfigApplicantToolStripMenuItem
            // 
            this.aboutDTIGuardianConfigApplicantToolStripMenuItem.Name = "aboutDTIGuardianConfigApplicantToolStripMenuItem";
            this.aboutDTIGuardianConfigApplicantToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.aboutDTIGuardianConfigApplicantToolStripMenuItem.Text = "About DTI Guardian Config Applicant";
            // 
            // testToolStripMenuItem
            // 
            this.testToolStripMenuItem.Name = "testToolStripMenuItem";
            this.testToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.testToolStripMenuItem.Text = "Test";
            // 
            // ValveVCUInfoBox
            // 
            this.ValveVCUInfoBox.BackColor = System.Drawing.Color.Honeydew;
            this.ValveVCUInfoBox.Controls.Add(this.label43);
            this.ValveVCUInfoBox.Controls.Add(this.label44);
            this.ValveVCUInfoBox.Controls.Add(this.label41);
            this.ValveVCUInfoBox.Controls.Add(this.label42);
            this.ValveVCUInfoBox.Controls.Add(this.label39);
            this.ValveVCUInfoBox.Controls.Add(this.label40);
            this.ValveVCUInfoBox.Controls.Add(this.label37);
            this.ValveVCUInfoBox.Controls.Add(this.label38);
            this.ValveVCUInfoBox.Controls.Add(this.ValveSNValue);
            this.ValveVCUInfoBox.Controls.Add(this.VCUSNValue);
            this.ValveVCUInfoBox.Controls.Add(this.ValveSNLabel);
            this.ValveVCUInfoBox.Controls.Add(this.VCUSNLabel);
            this.ValveVCUInfoBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ValveVCUInfoBox.ForeColor = System.Drawing.SystemColors.InfoText;
            this.ValveVCUInfoBox.Location = new System.Drawing.Point(800, 129);
            this.ValveVCUInfoBox.Name = "ValveVCUInfoBox";
            this.ValveVCUInfoBox.Size = new System.Drawing.Size(244, 169);
            this.ValveVCUInfoBox.TabIndex = 2;
            this.ValveVCUInfoBox.TabStop = false;
            this.ValveVCUInfoBox.Text = "Valve/VCU Information";
            // 
            // ValveSNValue
            // 
            this.ValveSNValue.AutoSize = true;
            this.ValveSNValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ValveSNValue.ForeColor = System.Drawing.SystemColors.InfoText;
            this.ValveSNValue.Location = new System.Drawing.Point(136, 29);
            this.ValveSNValue.Name = "ValveSNValue";
            this.ValveSNValue.Size = new System.Drawing.Size(94, 13);
            this.ValveSNValue.TabIndex = 3;
            this.ValveSNValue.Text = "XXX000000000";
            // 
            // VCUSNValue
            // 
            this.VCUSNValue.AutoSize = true;
            this.VCUSNValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VCUSNValue.ForeColor = System.Drawing.SystemColors.InfoText;
            this.VCUSNValue.Location = new System.Drawing.Point(136, 16);
            this.VCUSNValue.Name = "VCUSNValue";
            this.VCUSNValue.Size = new System.Drawing.Size(94, 13);
            this.VCUSNValue.TabIndex = 2;
            this.VCUSNValue.Text = "XXX000000000";
            // 
            // ValveSNLabel
            // 
            this.ValveSNLabel.AutoSize = true;
            this.ValveSNLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ValveSNLabel.ForeColor = System.Drawing.SystemColors.InfoText;
            this.ValveSNLabel.Location = new System.Drawing.Point(6, 29);
            this.ValveSNLabel.Name = "ValveSNLabel";
            this.ValveSNLabel.Size = new System.Drawing.Size(126, 13);
            this.ValveSNLabel.TabIndex = 1;
            this.ValveSNLabel.Text = "Valve Serial Number:";
            // 
            // ValveInteractionBox
            // 
            this.ValveInteractionBox.BackColor = System.Drawing.Color.Honeydew;
            this.ValveInteractionBox.Controls.Add(this.button5);
            this.ValveInteractionBox.Controls.Add(this.button4);
            this.ValveInteractionBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ValveInteractionBox.ForeColor = System.Drawing.SystemColors.InfoText;
            this.ValveInteractionBox.Location = new System.Drawing.Point(7, 254);
            this.ValveInteractionBox.Name = "ValveInteractionBox";
            this.ValveInteractionBox.Size = new System.Drawing.Size(220, 45);
            this.ValveInteractionBox.TabIndex = 4;
            this.ValveInteractionBox.TabStop = false;
            this.ValveInteractionBox.Text = "Valve Interaction";
            // 
            // StatusBox
            // 
            this.StatusBox.BackColor = System.Drawing.Color.Honeydew;
            this.StatusBox.Controls.Add(this.label36);
            this.StatusBox.Controls.Add(this.label24);
            this.StatusBox.Controls.Add(this.label23);
            this.StatusBox.Controls.Add(this.label22);
            this.StatusBox.Controls.Add(this.label21);
            this.StatusBox.Controls.Add(this.label20);
            this.StatusBox.Controls.Add(this.label19);
            this.StatusBox.Controls.Add(this.label18);
            this.StatusBox.Controls.Add(this.label17);
            this.StatusBox.Controls.Add(this.label16);
            this.StatusBox.Controls.Add(this.label15);
            this.StatusBox.Controls.Add(this.label14);
            this.StatusBox.Controls.Add(this.label13);
            this.StatusBox.Controls.Add(this.label12);
            this.StatusBox.Controls.Add(this.label11);
            this.StatusBox.Controls.Add(this.label10);
            this.StatusBox.Controls.Add(this.label9);
            this.StatusBox.Controls.Add(this.label8);
            this.StatusBox.Controls.Add(this.label7);
            this.StatusBox.Controls.Add(this.label6);
            this.StatusBox.Controls.Add(this.label5);
            this.StatusBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StatusBox.ForeColor = System.Drawing.SystemColors.InfoText;
            this.StatusBox.Location = new System.Drawing.Point(7, 305);
            this.StatusBox.Name = "StatusBox";
            this.StatusBox.Size = new System.Drawing.Size(513, 270);
            this.StatusBox.TabIndex = 5;
            this.StatusBox.TabStop = false;
            this.StatusBox.Text = "Status";
            // 
            // ValveDataBox
            // 
            this.ValveDataBox.BackColor = System.Drawing.Color.Honeydew;
            this.ValveDataBox.Controls.Add(this.label35);
            this.ValveDataBox.Controls.Add(this.label34);
            this.ValveDataBox.Controls.Add(this.label33);
            this.ValveDataBox.Controls.Add(this.label32);
            this.ValveDataBox.Controls.Add(this.label31);
            this.ValveDataBox.Controls.Add(this.label30);
            this.ValveDataBox.Controls.Add(this.label29);
            this.ValveDataBox.Controls.Add(this.label28);
            this.ValveDataBox.Controls.Add(this.label27);
            this.ValveDataBox.Controls.Add(this.label26);
            this.ValveDataBox.Controls.Add(this.label25);
            this.ValveDataBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ValveDataBox.ForeColor = System.Drawing.SystemColors.InfoText;
            this.ValveDataBox.Location = new System.Drawing.Point(233, 38);
            this.ValveDataBox.Name = "ValveDataBox";
            this.ValveDataBox.Size = new System.Drawing.Size(286, 260);
            this.ValveDataBox.TabIndex = 6;
            this.ValveDataBox.TabStop = false;
            this.ValveDataBox.Text = "Valve Data";
            // 
            // ConnectionBox
            // 
            this.ConnectionBox.BackColor = System.Drawing.Color.Honeydew;
            this.ConnectionBox.Controls.Add(this.label4);
            this.ConnectionBox.Controls.Add(this.label3);
            this.ConnectionBox.Controls.Add(this.label2);
            this.ConnectionBox.Controls.Add(this.button3);
            this.ConnectionBox.Controls.Add(this.label1);
            this.ConnectionBox.Controls.Add(this.button2);
            this.ConnectionBox.Controls.Add(this.button1);
            this.ConnectionBox.Controls.Add(this.radioButton3);
            this.ConnectionBox.Controls.Add(this.radioButton2);
            this.ConnectionBox.Controls.Add(this.radioButton1);
            this.ConnectionBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConnectionBox.ForeColor = System.Drawing.SystemColors.InfoText;
            this.ConnectionBox.Location = new System.Drawing.Point(525, 38);
            this.ConnectionBox.Name = "ConnectionBox";
            this.ConnectionBox.Size = new System.Drawing.Size(270, 260);
            this.ConnectionBox.TabIndex = 7;
            this.ConnectionBox.TabStop = false;
            this.ConnectionBox.Text = "Connection";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(138, 177);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(126, 33);
            this.button2.TabIndex = 4;
            this.button2.Text = "Disconnect";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(6, 177);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(126, 33);
            this.button1.TabIndex = 3;
            this.button1.Text = "Connect";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(17, 60);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Padding = new System.Windows.Forms.Padding(4);
            this.radioButton3.Size = new System.Drawing.Size(101, 25);
            this.radioButton3.TabIndex = 2;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "Serial Cable";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(17, 82);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Padding = new System.Windows.Forms.Padding(4);
            this.radioButton2.Size = new System.Drawing.Size(76, 25);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Remote";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(17, 40);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Padding = new System.Windows.Forms.Padding(4);
            this.radioButton1.Size = new System.Drawing.Size(87, 25);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Bluetooth";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // ConfigBox
            // 
            this.ConfigBox.BackColor = System.Drawing.Color.Honeydew;
            this.ConfigBox.Controls.Add(this.checkBox4);
            this.ConfigBox.Controls.Add(this.checkBox5);
            this.ConfigBox.Controls.Add(this.checkBox6);
            this.ConfigBox.Controls.Add(this.checkBox3);
            this.ConfigBox.Controls.Add(this.checkBox2);
            this.ConfigBox.Controls.Add(this.checkBox1);
            this.ConfigBox.Controls.Add(this.label65);
            this.ConfigBox.Controls.Add(this.label66);
            this.ConfigBox.Controls.Add(this.label67);
            this.ConfigBox.Controls.Add(this.label68);
            this.ConfigBox.Controls.Add(this.label69);
            this.ConfigBox.Controls.Add(this.label70);
            this.ConfigBox.Controls.Add(this.label71);
            this.ConfigBox.Controls.Add(this.label72);
            this.ConfigBox.Controls.Add(this.label73);
            this.ConfigBox.Controls.Add(this.label74);
            this.ConfigBox.Controls.Add(this.label55);
            this.ConfigBox.Controls.Add(this.label56);
            this.ConfigBox.Controls.Add(this.label57);
            this.ConfigBox.Controls.Add(this.label58);
            this.ConfigBox.Controls.Add(this.label59);
            this.ConfigBox.Controls.Add(this.label60);
            this.ConfigBox.Controls.Add(this.label61);
            this.ConfigBox.Controls.Add(this.label62);
            this.ConfigBox.Controls.Add(this.label63);
            this.ConfigBox.Controls.Add(this.label64);
            this.ConfigBox.Controls.Add(this.label50);
            this.ConfigBox.Controls.Add(this.label51);
            this.ConfigBox.Controls.Add(this.label52);
            this.ConfigBox.Controls.Add(this.label53);
            this.ConfigBox.Controls.Add(this.label54);
            this.ConfigBox.Controls.Add(this.label49);
            this.ConfigBox.Controls.Add(this.label48);
            this.ConfigBox.Controls.Add(this.label47);
            this.ConfigBox.Controls.Add(this.label46);
            this.ConfigBox.Controls.Add(this.label45);
            this.ConfigBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConfigBox.ForeColor = System.Drawing.SystemColors.InfoText;
            this.ConfigBox.Location = new System.Drawing.Point(525, 305);
            this.ConfigBox.Name = "ConfigBox";
            this.ConfigBox.Size = new System.Drawing.Size(519, 270);
            this.ConfigBox.TabIndex = 8;
            this.ConfigBox.TabStop = false;
            this.ConfigBox.Text = "Configuration";
            // 
            // UserInfoBox
            // 
            this.UserInfoBox.BackColor = System.Drawing.Color.Honeydew;
            this.UserInfoBox.Controls.Add(this.UserTypeValue);
            this.UserInfoBox.Controls.Add(this.UserValue);
            this.UserInfoBox.Controls.Add(this.UserTypeLabel);
            this.UserInfoBox.Controls.Add(this.UserLabel);
            this.UserInfoBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserInfoBox.ForeColor = System.Drawing.SystemColors.InfoText;
            this.UserInfoBox.Location = new System.Drawing.Point(800, 62);
            this.UserInfoBox.Name = "UserInfoBox";
            this.UserInfoBox.Size = new System.Drawing.Size(244, 61);
            this.UserInfoBox.TabIndex = 9;
            this.UserInfoBox.TabStop = false;
            this.UserInfoBox.Text = "User Information";
            // 
            // UserTypeValue
            // 
            this.UserTypeValue.AutoSize = true;
            this.UserTypeValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserTypeValue.ForeColor = System.Drawing.SystemColors.InfoText;
            this.UserTypeValue.Location = new System.Drawing.Point(76, 35);
            this.UserTypeValue.Name = "UserTypeValue";
            this.UserTypeValue.Size = new System.Drawing.Size(88, 13);
            this.UserTypeValue.TabIndex = 3;
            this.UserTypeValue.Text = "Not Logged In";
            // 
            // UserValue
            // 
            this.UserValue.AutoSize = true;
            this.UserValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserValue.ForeColor = System.Drawing.SystemColors.InfoText;
            this.UserValue.Location = new System.Drawing.Point(76, 22);
            this.UserValue.Name = "UserValue";
            this.UserValue.Size = new System.Drawing.Size(53, 13);
            this.UserValue.TabIndex = 2;
            this.UserValue.Text = "No User";
            // 
            // UserTypeLabel
            // 
            this.UserTypeLabel.AutoSize = true;
            this.UserTypeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserTypeLabel.ForeColor = System.Drawing.SystemColors.InfoText;
            this.UserTypeLabel.Location = new System.Drawing.Point(11, 35);
            this.UserTypeLabel.Name = "UserTypeLabel";
            this.UserTypeLabel.Size = new System.Drawing.Size(69, 13);
            this.UserTypeLabel.TabIndex = 1;
            this.UserTypeLabel.Text = "User Type:";
            // 
            // UserLabel
            // 
            this.UserLabel.AutoSize = true;
            this.UserLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserLabel.ForeColor = System.Drawing.SystemColors.InfoText;
            this.UserLabel.Location = new System.Drawing.Point(11, 22);
            this.UserLabel.Name = "UserLabel";
            this.UserLabel.Size = new System.Drawing.Size(37, 13);
            this.UserLabel.TabIndex = 0;
            this.UserLabel.Text = "User:";
            // 
            // FirmwareLabel
            // 
            this.FirmwareLabel.AutoSize = true;
            this.FirmwareLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FirmwareLabel.ForeColor = System.Drawing.SystemColors.InfoText;
            this.FirmwareLabel.Location = new System.Drawing.Point(806, 38);
            this.FirmwareLabel.Name = "FirmwareLabel";
            this.FirmwareLabel.Size = new System.Drawing.Size(61, 13);
            this.FirmwareLabel.TabIndex = 10;
            this.FirmwareLabel.Text = "Firmware:";
            // 
            // HardwareLabel
            // 
            this.HardwareLabel.AutoSize = true;
            this.HardwareLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HardwareLabel.ForeColor = System.Drawing.SystemColors.InfoText;
            this.HardwareLabel.Location = new System.Drawing.Point(899, 38);
            this.HardwareLabel.Name = "HardwareLabel";
            this.HardwareLabel.Size = new System.Drawing.Size(65, 13);
            this.HardwareLabel.TabIndex = 11;
            this.HardwareLabel.Text = "Hardware:";
            // 
            // HardwareVersion
            // 
            this.HardwareVersion.AutoSize = true;
            this.HardwareVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HardwareVersion.ForeColor = System.Drawing.SystemColors.InfoText;
            this.HardwareVersion.Location = new System.Drawing.Point(956, 38);
            this.HardwareVersion.Name = "HardwareVersion";
            this.HardwareVersion.Size = new System.Drawing.Size(25, 13);
            this.HardwareVersion.TabIndex = 12;
            this.HardwareVersion.Text = "0.0";
            // 
            // FirmwareVersion
            // 
            this.FirmwareVersion.AutoSize = true;
            this.FirmwareVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FirmwareVersion.ForeColor = System.Drawing.SystemColors.InfoText;
            this.FirmwareVersion.Location = new System.Drawing.Point(864, 38);
            this.FirmwareVersion.Name = "FirmwareVersion";
            this.FirmwareVersion.Size = new System.Drawing.Size(25, 13);
            this.FirmwareVersion.TabIndex = 13;
            this.FirmwareVersion.Text = "0.0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label1.Location = new System.Drawing.Point(14, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Connection Type";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(6, 216);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(258, 33);
            this.button3.TabIndex = 5;
            this.button3.Text = "Save Settings to VCU (F4)";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label2.Location = new System.Drawing.Point(161, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 25);
            this.label2.TabIndex = 6;
            this.label2.Text = "AUTO";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label3.Location = new System.Drawing.Point(142, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Connection Status";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Red;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label4.Location = new System.Drawing.Point(145, 83);
            this.label4.Name = "label4";
            this.label4.Padding = new System.Windows.Forms.Padding(5, 4, 5, 5);
            this.label4.Size = new System.Drawing.Size(104, 24);
            this.label4.TabIndex = 8;
            this.label4.Text = "Not Connected";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label5.Location = new System.Drawing.Point(26, 27);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "User:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label6.Location = new System.Drawing.Point(26, 50);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "User:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label7.Location = new System.Drawing.Point(26, 78);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "User:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label8.Location = new System.Drawing.Point(56, 65);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "User:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label9.Location = new System.Drawing.Point(56, 91);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "User:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label10.Location = new System.Drawing.Point(26, 149);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(37, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "User:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label11.Location = new System.Drawing.Point(26, 162);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(37, 13);
            this.label11.TabIndex = 15;
            this.label11.Text = "User:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label12.Location = new System.Drawing.Point(26, 175);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(37, 13);
            this.label12.TabIndex = 16;
            this.label12.Text = "User:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label13.Location = new System.Drawing.Point(26, 188);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(37, 13);
            this.label13.TabIndex = 17;
            this.label13.Text = "User:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label14.Location = new System.Drawing.Point(69, 162);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(37, 13);
            this.label14.TabIndex = 18;
            this.label14.Text = "User:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label15.Location = new System.Drawing.Point(69, 175);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(37, 13);
            this.label15.TabIndex = 19;
            this.label15.Text = "User:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label16.Location = new System.Drawing.Point(69, 188);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(37, 13);
            this.label16.TabIndex = 20;
            this.label16.Text = "User:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label17.Location = new System.Drawing.Point(158, 149);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(37, 13);
            this.label17.TabIndex = 21;
            this.label17.Text = "User:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label18.Location = new System.Drawing.Point(158, 162);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(37, 13);
            this.label18.TabIndex = 22;
            this.label18.Text = "User:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label19.Location = new System.Drawing.Point(201, 162);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(37, 13);
            this.label19.TabIndex = 23;
            this.label19.Text = "User:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label20.Location = new System.Drawing.Point(158, 175);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(37, 13);
            this.label20.TabIndex = 24;
            this.label20.Text = "User:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label21.Location = new System.Drawing.Point(201, 175);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(37, 13);
            this.label21.TabIndex = 25;
            this.label21.Text = "User:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label22.Location = new System.Drawing.Point(158, 188);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(37, 13);
            this.label22.TabIndex = 26;
            this.label22.Text = "User:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label23.Location = new System.Drawing.Point(201, 188);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(37, 13);
            this.label23.TabIndex = 27;
            this.label23.Text = "User:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label24.Location = new System.Drawing.Point(183, 27);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(37, 13);
            this.label24.TabIndex = 28;
            this.label24.Text = "User:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label25.Location = new System.Drawing.Point(17, 203);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(37, 13);
            this.label25.TabIndex = 29;
            this.label25.Text = "User:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label26.Location = new System.Drawing.Point(17, 226);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(37, 13);
            this.label26.TabIndex = 30;
            this.label26.Text = "User:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label27.Location = new System.Drawing.Point(60, 226);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(37, 13);
            this.label27.TabIndex = 31;
            this.label27.Text = "User:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label28.Location = new System.Drawing.Point(103, 226);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(37, 13);
            this.label28.TabIndex = 32;
            this.label28.Text = "User:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label29.Location = new System.Drawing.Point(146, 226);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(37, 13);
            this.label29.TabIndex = 33;
            this.label29.Text = "User:";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label30.Location = new System.Drawing.Point(189, 226);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(37, 13);
            this.label30.TabIndex = 34;
            this.label30.Text = "User:";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label31.Location = new System.Drawing.Point(232, 226);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(37, 13);
            this.label31.TabIndex = 35;
            this.label31.Text = "User:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label32.Location = new System.Drawing.Point(17, 131);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(37, 13);
            this.label32.TabIndex = 36;
            this.label32.Text = "User:";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label33.Location = new System.Drawing.Point(17, 144);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(37, 13);
            this.label33.TabIndex = 37;
            this.label33.Text = "User:";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label34.Location = new System.Drawing.Point(60, 170);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(37, 13);
            this.label34.TabIndex = 38;
            this.label34.Text = "User:";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label35.Location = new System.Drawing.Point(60, 183);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(37, 13);
            this.label35.TabIndex = 39;
            this.label35.Text = "User:";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label36.Location = new System.Drawing.Point(329, 162);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(37, 13);
            this.label36.TabIndex = 40;
            this.label36.Text = "User:";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(18, 16);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 9;
            this.button4.Text = "button4";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(120, 16);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 10;
            this.button5.Text = "button5";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label37.Location = new System.Drawing.Point(136, 42);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(94, 13);
            this.label37.TabIndex = 5;
            this.label37.Text = "XXX000000000";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label38.Location = new System.Drawing.Point(6, 42);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(126, 13);
            this.label38.TabIndex = 4;
            this.label38.Text = "Valve Serial Number:";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label39.Location = new System.Drawing.Point(136, 55);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(94, 13);
            this.label39.TabIndex = 7;
            this.label39.Text = "XXX000000000";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label40.Location = new System.Drawing.Point(6, 55);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(126, 13);
            this.label40.TabIndex = 6;
            this.label40.Text = "Valve Serial Number:";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label41.Location = new System.Drawing.Point(136, 68);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(94, 13);
            this.label41.TabIndex = 9;
            this.label41.Text = "XXX000000000";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label42.Location = new System.Drawing.Point(6, 68);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(126, 13);
            this.label42.TabIndex = 8;
            this.label42.Text = "Valve Serial Number:";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label43.Location = new System.Drawing.Point(136, 81);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(94, 13);
            this.label43.TabIndex = 11;
            this.label43.Text = "XXX000000000";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label44.Location = new System.Drawing.Point(6, 81);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(126, 13);
            this.label44.TabIndex = 10;
            this.label44.Text = "Valve Serial Number:";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label45.Location = new System.Drawing.Point(14, 27);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(37, 13);
            this.label45.TabIndex = 41;
            this.label45.Text = "User:";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label46.Location = new System.Drawing.Point(14, 50);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(37, 13);
            this.label46.TabIndex = 42;
            this.label46.Text = "User:";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label47.Location = new System.Drawing.Point(14, 78);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(37, 13);
            this.label47.TabIndex = 43;
            this.label47.Text = "User:";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label48.Location = new System.Drawing.Point(14, 107);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(37, 13);
            this.label48.TabIndex = 44;
            this.label48.Text = "User:";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label49.Location = new System.Drawing.Point(14, 130);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(37, 13);
            this.label49.TabIndex = 45;
            this.label49.Text = "User:";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label50.Location = new System.Drawing.Point(57, 130);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(37, 13);
            this.label50.TabIndex = 50;
            this.label50.Text = "User:";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label51.Location = new System.Drawing.Point(57, 107);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(37, 13);
            this.label51.TabIndex = 49;
            this.label51.Text = "User:";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label52.Location = new System.Drawing.Point(57, 78);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(37, 13);
            this.label52.TabIndex = 48;
            this.label52.Text = "User:";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label53.Location = new System.Drawing.Point(57, 50);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(37, 13);
            this.label53.TabIndex = 47;
            this.label53.Text = "User:";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label54.Location = new System.Drawing.Point(57, 27);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(37, 13);
            this.label54.TabIndex = 46;
            this.label54.Text = "User:";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label55.Location = new System.Drawing.Point(181, 130);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(37, 13);
            this.label55.TabIndex = 60;
            this.label55.Text = "User:";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label56.Location = new System.Drawing.Point(181, 107);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(37, 13);
            this.label56.TabIndex = 59;
            this.label56.Text = "User:";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label57.Location = new System.Drawing.Point(181, 78);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(37, 13);
            this.label57.TabIndex = 58;
            this.label57.Text = "User:";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label58.Location = new System.Drawing.Point(181, 50);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(37, 13);
            this.label58.TabIndex = 57;
            this.label58.Text = "User:";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label59.Location = new System.Drawing.Point(181, 27);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(37, 13);
            this.label59.TabIndex = 56;
            this.label59.Text = "User:";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label60.Location = new System.Drawing.Point(138, 130);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(37, 13);
            this.label60.TabIndex = 55;
            this.label60.Text = "User:";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label61.Location = new System.Drawing.Point(138, 107);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(37, 13);
            this.label61.TabIndex = 54;
            this.label61.Text = "User:";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label62.Location = new System.Drawing.Point(138, 78);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(37, 13);
            this.label62.TabIndex = 53;
            this.label62.Text = "User:";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label63.Location = new System.Drawing.Point(138, 50);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(37, 13);
            this.label63.TabIndex = 52;
            this.label63.Text = "User:";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label64.Location = new System.Drawing.Point(138, 27);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(37, 13);
            this.label64.TabIndex = 51;
            this.label64.Text = "User:";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label65.Location = new System.Drawing.Point(315, 130);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(37, 13);
            this.label65.TabIndex = 70;
            this.label65.Text = "User:";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label66.Location = new System.Drawing.Point(315, 107);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(37, 13);
            this.label66.TabIndex = 69;
            this.label66.Text = "User:";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label67.Location = new System.Drawing.Point(315, 78);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(37, 13);
            this.label67.TabIndex = 68;
            this.label67.Text = "User:";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label68.Location = new System.Drawing.Point(315, 50);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(37, 13);
            this.label68.TabIndex = 67;
            this.label68.Text = "User:";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label69.Location = new System.Drawing.Point(315, 27);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(37, 13);
            this.label69.TabIndex = 66;
            this.label69.Text = "User:";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label70.Location = new System.Drawing.Point(272, 130);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(37, 13);
            this.label70.TabIndex = 65;
            this.label70.Text = "User:";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label71.Location = new System.Drawing.Point(272, 107);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(37, 13);
            this.label71.TabIndex = 64;
            this.label71.Text = "User:";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label72.Location = new System.Drawing.Point(272, 78);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(37, 13);
            this.label72.TabIndex = 63;
            this.label72.Text = "User:";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label73.Location = new System.Drawing.Point(272, 50);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(37, 13);
            this.label73.TabIndex = 62;
            this.label73.Text = "User:";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label74.Location = new System.Drawing.Point(272, 27);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(37, 13);
            this.label74.TabIndex = 61;
            this.label74.Text = "User:";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(32, 184);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(89, 17);
            this.checkBox1.TabIndex = 71;
            this.checkBox1.Text = "checkBox1";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(32, 207);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(89, 17);
            this.checkBox2.TabIndex = 72;
            this.checkBox2.Text = "checkBox2";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(32, 230);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(89, 17);
            this.checkBox3.TabIndex = 73;
            this.checkBox3.Text = "checkBox3";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(145, 230);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(89, 17);
            this.checkBox4.TabIndex = 76;
            this.checkBox4.Text = "checkBox4";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(145, 207);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(89, 17);
            this.checkBox5.TabIndex = 75;
            this.checkBox5.Text = "checkBox5";
            this.checkBox5.UseVisualStyleBackColor = true;
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Location = new System.Drawing.Point(145, 184);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(89, 17);
            this.checkBox6.TabIndex = 74;
            this.checkBox6.Text = "checkBox6";
            this.checkBox6.UseVisualStyleBackColor = true;
            // 
            // MainConfigWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.ClientSize = new System.Drawing.Size(1053, 587);
            this.Controls.Add(this.FirmwareVersion);
            this.Controls.Add(this.HardwareVersion);
            this.Controls.Add(this.HardwareLabel);
            this.Controls.Add(this.FirmwareLabel);
            this.Controls.Add(this.UserInfoBox);
            this.Controls.Add(this.ConfigBox);
            this.Controls.Add(this.ConnectionBox);
            this.Controls.Add(this.ValveDataBox);
            this.Controls.Add(this.StatusBox);
            this.Controls.Add(this.ValveInteractionBox);
            this.Controls.Add(this.ValveVCUInfoBox);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainConfigWindow";
            this.Text = "DTI Guardian Config Application";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ValveVCUInfoBox.ResumeLayout(false);
            this.ValveVCUInfoBox.PerformLayout();
            this.ValveInteractionBox.ResumeLayout(false);
            this.StatusBox.ResumeLayout(false);
            this.StatusBox.PerformLayout();
            this.ValveDataBox.ResumeLayout(false);
            this.ValveDataBox.PerformLayout();
            this.ConnectionBox.ResumeLayout(false);
            this.ConnectionBox.PerformLayout();
            this.ConfigBox.ResumeLayout(false);
            this.ConfigBox.PerformLayout();
            this.UserInfoBox.ResumeLayout(false);
            this.UserInfoBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label VCUSNLabel;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem templateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inputsOutputsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem valveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pTOToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rPMToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem remoteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eventLogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem conditionCodesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tiltToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem connectF1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem openValveF2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeValveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToVCUToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zeroCountToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem systemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem firmwareUpdateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configBlutoothModuleToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem configExternalModuleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem auxInputsCalibrationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetVCUToDefaultsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enableFeaturesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem proToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eliteToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem saveVCUDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem auxiliary1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem auxiliary2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem auxiliary3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem auxiliary4ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem auxiliary5ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem auxiliary6ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem auxiliary7ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem auxiliary8ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem keyfobToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem masterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpTopicsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem checkForUpdatesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutDTIGuardianConfigApplicantToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testToolStripMenuItem;
        private System.Windows.Forms.GroupBox ValveVCUInfoBox;
        private System.Windows.Forms.Label ValveSNLabel;
        private System.Windows.Forms.GroupBox ValveInteractionBox;
        private System.Windows.Forms.GroupBox StatusBox;
        private System.Windows.Forms.GroupBox ValveDataBox;
        private System.Windows.Forms.GroupBox ConnectionBox;
        private System.Windows.Forms.GroupBox ConfigBox;
        private System.Windows.Forms.GroupBox UserInfoBox;
        private System.Windows.Forms.Label UserTypeValue;
        private System.Windows.Forms.Label UserValue;
        private System.Windows.Forms.Label UserTypeLabel;
        private System.Windows.Forms.Label UserLabel;
        private System.Windows.Forms.Label FirmwareLabel;
        private System.Windows.Forms.Label HardwareLabel;
        private System.Windows.Forms.Label HardwareVersion;
        private System.Windows.Forms.Label FirmwareVersion;
        private System.Windows.Forms.Label ValveSNValue;
        private System.Windows.Forms.Label VCUSNValue;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label45;
    }
}

